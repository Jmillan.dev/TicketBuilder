﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TicketBuilder
{
    public enum Alignment
    {
        Left,
        Center,
        Right
    }

    public class Ticket
    {
        private readonly StringBuilder _str = new StringBuilder();
        private readonly short _maxChar;

        public Ticket(short maxChar = 35)
        {
            _maxChar = maxChar;
        }

        /// <summary>
        /// Adds one or many empty lines
        /// </summary>
        /// <param name="number"> The number of empty lines. By default is just one line</param>
        public void AddEmptyLine(int number = 1) => _str.Append(new string('\n', number));

        /// <summary>
        /// Adds a Line repeating the given character
        /// </summary>
        /// <param name="chr"> The character to use. By default is `*`</param>
        public void AddLine(char chr = '*') => _str.AppendLine(new string(chr, _maxChar));

        /// <summary>
        /// Split Text with the given string and size with a serie of rules
        /// </summary>
        /// <param name="str">the text to be spliced</param>
        /// <param name="limit">The lenght in character size that each line can have</param>
        /// <returns>Return a list of each line generated from the string</returns>
        public static List<string> SplitText(string str, int limit)
        {
            var junk = string.Copy(str);
            return SplitText(ref junk, limit, 0);
        }
        
        public static List<string> SplitText(ref string str, int limit, int line)
        {
            var lines = new List<string>();
            var strCopy = string.Copy(str);
            var temporalStr = "";
            var junk = "";

            var wordString = "";
            for (var i = 0; i < strCopy.Length; i++)
            {
                // break this
                if (line != 0 && lines.Count >= line) return lines;
                
                var c = strCopy[i];
                junk += c;

                if (c == '\n')
                {
                    StringInsertInto(ref temporalStr, ref wordString);
                    LineInsertInto(lines,ref temporalStr, ref str, 1);
                    junk = "";
                    continue;
                }

                if (c != ' ') wordString += c;
                else StringInsertInto(ref temporalStr, ref wordString, "{0}{1} ");

                var end = i + 1 >= strCopy.Length;
                // Another condition
                if (junk.Length != limit && !end) continue;
                //we still have a word inside... then we just drop it
                if (end) StringInsertInto(ref temporalStr, ref wordString);
                if (wordString.Length >= limit) StringInsertInto(ref temporalStr, ref wordString);
                LineInsertInto(lines,ref temporalStr, ref str);
                junk = wordString; // Start from the wordString if there are any
            }

            return lines;
        }

        public static string AlignText(string str, int size, Alignment alignment) =>
            alignment switch
            {
                Alignment.Left => str,
                Alignment.Center => $"{new string(' ', (int) ((size - str.Length) / 2d))}{str}",
                Alignment.Right => $"{new string(' ', size - str.Length)}{str}",
                _ => throw new ArgumentOutOfRangeException(nameof(alignment), alignment, null)
            };

        public static List<string> AlignTextList(List<string> strList, int size, Alignment alignment) =>
            strList.Select(e => AlignText(e, size, alignment)).ToList();

        public string AddText(string text, Alignment alignment = Alignment.Left, short limit = 0)
        {
            limit = limit == 0 ? _maxChar : limit;
            var lines = AlignTextList(SplitText(text, limit), limit, alignment);
            return _str.AppendLine(string.Join("\n", lines.ToArray())).ToString();
        }

        public string AddRow(List<string> textList, Table tableInfo) => AddRow(textList.ToArray(), tableInfo);

        public string AddRow(string[] textList, Table tableInfo)
        {
            // Column: [ 1, 2, 3 ] Each column is processed and generates a single Row per iteration.
            // This is so we can have the method of grow working inside.
            // The remain is saved for the next iteration each iteration completes until the textList is empty.

            // Initializes our little custom grid
            var stringTable = new List<string>[tableInfo.Columns.Count];
            for (var i = 0; i < stringTable.Length; i++)
                stringTable[i] = new List<string>();

            // Makes sure that this loop is not infinite if something goes wrong
            var rowIndexMax = 999;
            var rowIndex = 0;

            // Break the while when the textList given has no text left
            while (textList.Aggregate((a, e) => a + e).Length > 0)
            {
                for (var i = 0; i < tableInfo.Columns.Count; i++)
                {
                    var column = tableInfo.Columns[i];
                    if (textList.Length > i)
                    {
                        if (textList[i].Length == 0)
                        {
                            if (stringTable[i].Count < rowIndex)
                                stringTable[i].Add("");
                            continue; // Jump to the next sequence if the text is nothing
                        }

                        // If this is a grow column it checks that if the neighbor column's rows have text... if not...
                        // Then it takes them and use those places to fill it with information
                        if (column.HasGrow())
                        {
                            // Check if neighbor columns and theirs have text and in the rows
                            // "if the information has been process already"
                            var lColumn = column;
                            var lTotalSize = 0;
                            var rColumn = column;
                            var rTotalSize = 0;
                            // Get the far left Column
                            if (column.GrowLeft)
                            {
                                for (var c = column.before; c != null; c = c.before)
                                {
                                    // Compare all left neighbors
                                    // Check if their row is empty or not

                                    var cList = stringTable[c.index];
                                    var content = cList[rowIndex];

                                    if (!string.IsNullOrEmpty(content)) break;

                                    lColumn = c;
                                    lTotalSize += c.Size + c.marginRight;
                                }
                            }

                            // Get the far right Column
                            if (column.GrowRight)
                            {
                                for (var c = column.next; c != null; c = c.next)
                                {
                                    // If the next row still have text in the pool... Then it means that
                                    // we cannot grow there
                                    var content = textList.Length > c.index ? textList[c.index] : null;

                                    // if it is not null or not empty then we cannot continue
                                    if (!string.IsNullOrEmpty(content)) break;

                                    rColumn = c;
                                    rTotalSize += c.Size + c.marginRight;
                                }
                            }

                            // Maybe it can grow... or not... that is why we can continue the instruction or not
                            if (rColumn != column || lColumn != column)
                            {
                                // do some calculations of where and how
                                var totalSize = rTotalSize + lTotalSize + column.Size;
                                var textComplete = SplitText(ref textList[i], totalSize, 1)[0];
                                textComplete = AlignText(textComplete, totalSize, column.Alignment);

                                // Split the result and put them between the remaining places
                                for (var c = lColumn; c != rColumn.next; c = c.next)
                                {
                                    string text;
                                    var rowList = stringTable[c.index];

                                    if (string.IsNullOrEmpty(textComplete)) text = "";
                                    else
                                    {
                                        var position = c.Size + c.marginRight;
                                        if (position >= textComplete.Length)
                                            position = textComplete.Length;
                                        text = textComplete.Substring(0, position);
                                        textComplete = textComplete.Substring(position);
                                    }

                                    if (rowList.Count > rowIndex) rowList[rowIndex] = text;
                                    else rowList.Add(text);
                                }
                                continue;
                            }
                        }

                        // Do it one at a time
                        var textSpliced = SplitText(ref textList[i], column.Size, 1)[0];
                        textSpliced = AlignText(textSpliced, column.Size, column.Alignment);
                        stringTable[i].Add(textSpliced);
                    }
                    else stringTable[i].Add("");
                }

                if (rowIndex++ > rowIndexMax) break;
            }

            // Get max row size from this
            // |
            // v
            // ...ion: [ a ...
            // [ / ...
            // This part, How deep it goes

            var maxLength = stringTable
                .Aggregate<List<string>, short>(0, (current, list) => (short) Math.Max(list.Count, current));

            // Join all together now
            var resultList = new List<string>();
            for (short layer = 0, loopTo1 = maxLength; layer < loopTo1; layer++)
            {
                var resultText = "";
                for (short index = 0, loopTo2 = (short)(tableInfo.Columns.Count - 1); index <= loopTo2; index++)
                {
                    var column = tableInfo.Columns[index];
                    var row = stringTable[index];
                    var cell = row.Count > layer ? row[layer] : null;
                    if (string.IsNullOrEmpty(cell))
                        resultText += new string(' ', column.Size + column.marginRight);
                    else
                        // Adds the remaining spaces if the lenght of this doesn't combine with the lenght of the column
                        resultText += cell + new string(' ', column.Size - cell.Length + column.marginRight);
                }

                resultList.Add(resultText);
            }

            return _str.AppendLine(string.Join("\n", resultList.ToArray())).ToString();
        }

        public string GetString() => _str.ToString();
        
        
        // UTIL
        private static void StringInsertInto(ref string target, ref string source, string format = "{0}{1}")
        {
            target = string.Format(format, target, source);
            source = "";
        }

        private static void LineInsertInto(ICollection<string> list, ref string result, ref string original, int more=0)
        {
            list.Add(result);
            original = original.Substring(result.Length + more);
            result = "";
        }
    }
}