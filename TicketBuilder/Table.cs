using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace TicketBuilder
{
    public class Table
    {
        public class Column
        {
            public int index;
            public short Size;
            public Alignment Alignment;
            public short marginRight;
            public bool GrowLeft;
            public bool GrowRight;
            public Column next;
            public Column before;

            public bool HasGrow() => GrowLeft || GrowRight;

            public Column(short size, Alignment alignment, bool grow = false)
            {
                Size = size;
                Alignment = alignment;
                marginRight = 1;
                GrowRight = GrowLeft = grow;
            }

            public Column(short size, Alignment alignment, bool growLeft, bool growRight)
            {
                Size = size;
                Alignment = alignment;
                marginRight = 1;
                GrowLeft = growLeft;
                GrowRight = growRight;
            }
        }

        public readonly List<Column> Columns = new List<Column>();

        private void NodeInsert(Column column)
        {
            if (!(Columns.Count <= 0))
            {
                var last = Columns.Last();
                column.before = last;
                last.next = column;
            }

            column.index = Columns.Count;
            Columns.Add(column);
        }

        public Column AddColumn(short size, Alignment alignment, bool growLeft, bool growRight)
        {
            var column = new Column(size, alignment, growLeft, growRight);
            NodeInsert(column);
            return column;
        }

        public Column AddColumn(short size, Alignment alignment, bool grow)
        {
            var column = new Column(size, alignment, grow);
            NodeInsert(column);
            return column;
        }
        
        public Column AddColumn(short size, Alignment alignment)
        {
            var column = new Column(size, alignment);
            NodeInsert(column);
            return column;
        }

        public void AddColumn(Column column) => NodeInsert(column);
    }
}